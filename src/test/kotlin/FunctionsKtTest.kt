import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class FunctionsKtTest

@Test

fun checkStringToList(): Unit {
    val emptyString = ""
    val emtyMutableList = MutableList(0){}
    assert(stringToList(emptyString)==emtyMutableList)
}


@Test

fun checkAttemptCheckEquals(){
    val stringWord1 = "comino"
    val stringWord2 = "comino"
    assertEquals(stringWord1,stringWord2)
}

@Test

fun checkAttemptCheckNotEquals(){
    val stringWord1 = "metas"
    val stringWord2 = "mates"
    assertNotEquals(stringWord1, stringWord2)
}
