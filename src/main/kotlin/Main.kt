import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.readLines

fun main() {
    val scanner = Scanner(System.`in`)
    header()
    println("Select lenguage:")
    var languageChar = scanner.next().single()
    while(!Character.isDigit(languageChar )  || languageChar.toString().toInt() !in 1..3 ){
    println(" \u001B[36m "+"                             CAT: 1    ENG: 2    SPA: 3 "+"\u001B[0m \nSelect lenguage:")
        languageChar = scanner.next().single()
    }
    val language = languageChar.toString().toInt()
    var partida: String
    var stringMotIntent: String
    var motSecret: MutableList<String> = MutableList(5){""}
    var motIntent: MutableList<String>
    var intents: Int
    var intentsAnteriors  : MutableList<MutableList<String>>
    var teclat: MutableList<String>
    var win: Boolean
    var stringMotSecret: String


    printInstructions(language)

    partida = scanner.next()

    while(partida!="1"){
        if(partida=="2")printInstructions(language)
        else {
            when(language){
                1->println("Nomès pots introduir 1 o 2 !")
                2->println("You can only intrude  1 o 2 !")
                3->println("Solo puedes introducir 1 o 2 !")
            }
        }
        partida = scanner.next()
    }

    do{
        when(language){
            1->stringMotSecret= Path("./src/main/languages/wordsCat.txt").readLines().random()
            2->stringMotSecret= Path("./src/main/languages/wordsEng.txt").readLines().random()
            3->stringMotSecret= Path("./src/main/languages/wordsSpa.txt").readLines().random()
        }
        stringMotSecret = "cebra"
        intents = 6
        intentsAnteriors =  MutableList(6){ MutableList(5) { "" } }
        var name = ""
        while(name==""){
            when(language){
                1->println("Abans, introueix el vostre nom d'usuari:")
                2->println("First, intrude your username:")
                3->println("Primero, introduzca su nombre:")
            }
            name = scanner.next()
        }


        teclat = mutableListOf("Q","W","E","R","T","Y","U","I","O","P","Ç","A","S","D","F","G","H","J","K","L","Z","X","C","V","B","N","M")
        when(language){
            1->println("¡HOLA ${name}, COMENÇA LA PARTIDA! Introdueix el primer intent.")
            2->println("¡HEY ${name}, THE GAME STARTS! Intrude the first guess.")
            3->println("¡HOLA ${name}, EMPIEZA LA PARTIDA! Introduce el primer intento.")
        }
        do{

            when(language){
                1->println("Tens $intents intents")
                2->println("You have $intents attempts left")
                3->println("Te quedan $intents intentos")
            }


            printPreviousAttempts(intents, intentsAnteriors)

            printKeyboard(teclat)

            stringMotIntent= attemptFormatCheck(language)

            motIntent= colorAttempt(stringMotSecret,stringMotIntent,teclat).first

            teclat=colorAttempt(stringMotSecret,stringMotIntent,teclat).second

            attemptPrint(motIntent)

            win = (attemptCheck(stringMotIntent,stringMotSecret, language, intents))
            if(win)break
            updatePreviousAttempts(motIntent, intentsAnteriors, intents)
            intents--

        } while (intents>0)

         var partidaInt = repeatRankingOrEndGame(intents, language)

        partidaInt = if(partidaInt == 1) {
            ranking(intents, language, name, win, true)
        }else{
            ranking(intents, language, name, win, false)
        }
    }while(partidaInt==2)

    when(language){
        1->print("Ha estat un autèntic plaer jugar amb tú, torna quan vulguis.")
        2->print("It has been a real pleasure to play with you, come back whenever you want.")
        3->print("Ha sido un auténtico placer jugar contigo, vuelve cuando te apetezca.")
    }






}


