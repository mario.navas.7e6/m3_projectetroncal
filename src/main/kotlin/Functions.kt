import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.readLines
import kotlin.io.path.writeText


fun header(){
    print(  " \u001B[34m" + "\n"+
            "|\\    \\   _____     ____|\\    \\    ___|\\    \\   ___|\\    \\  |    |        ___|\\     \\        \n" +
            " | |    | /    /|   /     /\\    \\  |    |\\    \\ |    |\\    \\ |    |       |     \\     \\       \n" +
            " \\/     / |    ||  /     /  \\    \\ |    | |    ||    | |    ||    |       |     ,_____/|      \n" +
            " /     /_  \\   \\/ |     |    |    ||    |/____/ |    | |    ||    |  ____ |     \\--'\\_|/      \n" +
            "|     // \\  \\   \\ |     |    |    ||    |\\    \\ |    | |    ||    | |    ||     /___/|        \n" +
            "|    |/   \\ |    ||\\     \\  /    /||    | |    ||    | |    ||    | |    ||     \\____|\\       \n" +
            "|\\ ___/\\   \\|   /|| \\_____\\/____/ ||____| |____||____|/____/||____|/____/||____ '     /|      \n" +
            "| |   | \\______/ | \\ |    ||    | /|    | |    ||    /    | ||    |     |||    /_____/ |      \n" +
            " \\|___|/\\ |    | |  \\|____||____|/ |____| |____||____|____|/ |____|_____|/|____|     | /      \n" +
            "    \\(   \\|____|/      \\(    )/      \\(     )/    \\(    )/     \\(    )/     \\( |_____|/       \n" +
            "     '      )/          '    '        '     '      '    '       '    '       '    )/          \n" +
            "            '               "+" \u001B[36m "+" CAT: 1    ENG: 2    SPA: 3 "+" \u001B[34m"+"                       '           "+"\n"+"\u001B[0m")
}
/**
 * The functions have lenguage as parameter and depending on this parameter, it will print the game instructions in the corresponding language.
 * @author Mario Navas
 * @param language The selected lenguage
 */

fun printInstructions(language: Int){
    val verd = "\u001B[42m"
    val groc = "\u001B[43m"
    val reset = "\u001B[00m"

    when(language){
        1->println(" Endevina el mot en 6 intents.\n" + "\n" +
                " - Has d'introduir paraules de 5 lletres i fer clic a ENTER (↵).\n" +"\n" +
                " - Després de cada intent, el color de les lletres canviarà per indicar l'evolució de la partida, alguns exemples:\n" +"\n" +
                "                                "+verd+"P"+reset+"INTA                        "+"                        MA"+groc+"N"+reset+"EC                     "+"              LLATI\n"+
                "         La lletra P es troba en el "+"\u001B[32m"+"lloc correcte"+reset+" de la paraula,  "+"La paraula té la lletra N però en un "+"\u001B[33m"+"altre lloc"+reset+",  "+"La paraula no conté "+"\u001B[30m"+"ninguna lletra"+reset+".\n"+"\n" +
                " - Es poden repetir lletres.\n"+ "\n" +
                "Si voleu començar la partida, introduïu : 1")

        2->println(" Guess the word in 6 tries.\n" + "\n" +
                " - You must enter 5 letter words and click ENTER (↵).\n" +"\n" +
                " - After each attempt, the color of the letters will change to indicate the progress of the game, some examples:\n" +"\n" +
                "                                "+verd+"P"+reset+"INTA                        "+"                        MA"+groc+"N"+reset+"EC                     "+"              LLATI\n"+
                " The letter P is in the "+ "\u001B[32m"+"correct place"+reset+" of the word, "+"The word has the letter N but in a "+"\u001B[33m"+  " other place" +reset+", "+"The word does not contain "+"\u001B[30m"+"no letter"+reset+".\n"+"\n" +
                " - Letters may be repeated.\n"+ "\n" +
                "If you want to start the game, enter : 1" )

        3->println(" Adivina la palabra en 6 intentos.\n" + "\n"  +
                " - Tienes que introducir palabras de 5 letras y hacer clic en ENTER (↵).\n" +"\n" +
                " - Después de cada intento, el color de las letras cambiará para indicar la evolución de la partida, algunos ejemplos:\n" +"\n" +
                "                                "+verd+"P"+reset+"INTA                        "+"                        MA"+groc+"N"+reset+"EC                     "+"              LLATI\n"+
                " La letra P se encuentra en el "+"[32m"+"lugar correcto"+reset+" de la palabra, "+"La palabra tiene la letra N pero en un "+"\u001B[33m"+" otro lugar"+reset+", "+"La palabra no contiene "+"\u001B[30m"+"ninguna letra"+reset+".\n"+"\n" +                " - Es poden repetir lletres.\n"+ "\n" +
                " - Se pueden repetir letras.\n"+ "\n" +
                "Si desea empezar la partida, introduzca : 1")
    }


}

/**
 * The function converts a sting to a mutable list of uppercase strings
 * @author Mario Navas
 * @param string The sting you would like to convert
 * @return The mutable list of uppercase strings
 */

fun stringToList(string: String):MutableList<String>{
    val list: MutableList<String> = MutableList(5){""}
    for(i in 0.. 4){
        list[i] = string[i].toString().capitalize()

    }
    return(list)
}


/**
 * The function prints the previous attempts of the user in the current game
 * @param intents Number of attempts the user have made in the current stage of the game
 * @param intentsAnteriors Mutable list that contains the intent of each attempt
 */

fun printPreviousAttempts(intents:Int, intentsAnteriors: MutableList<MutableList<String>>){
    for(i in 0..6-intents){ //imprimir els intents anteriors
        for(j in 0..intentsAnteriors[i].lastIndex){
            print(intentsAnteriors[i][j])
        }
        println()
    }
    repeat(intents-2) {println()}
}

/**
 * The function prints a keyboard with the letters in different colors
 * @param teclat mutable list of stings with all the letters
 */

fun printKeyboard(teclat: MutableList<String>){
    for(i in 0..teclat.lastIndex){
        if( i == 19){
            println(teclat[i])
            print(" ")
        }
        else if(i ==9 ||  i == 26)println(teclat[i])
        else print(teclat[i])
    }
}

/**
 * @param
 * @return
 */

fun attemptFormatCheck(language: Int): String{
    var stringMotIntent: String
    val scanner = Scanner(System.`in`)
    do{
        stringMotIntent = scanner.next()

        var noLletra = false
        var noSize = false
        var noContent = false

        if(stringMotIntent.length!=5)noSize=true

        for(i in 0.. stringMotIntent.lastIndex) { //   For fa que llista2 agafi el nou valor introduit per l'usuari, te en compte que no poden haber caracters que no siguin lletres i que el mot ha de ser de cinc lletres
            if(!Character.isLetter(stringMotIntent[i])){
                noContent = true
                break
            }
        }

       if(noSize || noContent)noLletra=true

        if(noContent){
            when(language){
                1->println("Comte! Nomes s'accepten lletres")
                2->println("Be careful! Only letters are accepted")
                3->println("Cuidado! Solo se aceptan letaras")
            }
        }
        if(noSize){
            when(language){
                1->println("Comte! El mot ha de contindre cinc lletres")
                2->println("Be careful! The word must contain five leters")
                3->println("Cuidado! La palabra debe contener cinco letras")
            }

        }
        if(noLletra){
            when(language){
                1->println("Torna a provar-ho:")
                2->println("Try again:")
                3->println("Vuelve a probarlo:")
            }

        }

    }while(noLletra)

    return(stringMotIntent)
}

/**
 * The function is to color each intent to indicate which letters are correct, which are in the secret word but in the wrong position and which ones are not in the word
 * it also colors the keyboard depending on the letter state (green, yellow, black or not color)
 * @param stringMotIntent string with intent word
 * @param stringMotSecrets string with secret word
 * @param teclat mutable list of stings with all the letters
 * @return it returns a pair with the colored intent(MutableList<String>) and the colored teclat
 */

fun colorAttempt(stringMotSecret: String, stringMotIntent: String, teclat: MutableList<String>):Pair<MutableList<String>,MutableList<String>>{
    var lletra:Boolean
    val motSecret = stringToList(stringMotSecret)
    val motIntent = stringToList(stringMotIntent)
    val verd = "\u001B[42m"
    val groc = "\u001B[43m"
    val negre = "\u001B[40m"
    val reset = "\u001B[00m"
    for(i in 0..motIntent.lastIndex){
        lletra= false
        if(motSecret[i]==motIntent[i] && teclat.indexOf(motIntent[i].capitalize()) != -1) teclat[teclat.indexOf(motIntent[i].capitalize())]= verd + motIntent[i] + reset //afageixo el color verd a la lletra en la llista teclat
        if(motSecret[i]==motIntent[i]){
            motIntent[i]= verd + motIntent[i] + reset
            lletra=true

        }
        else for(j in 0..motIntent.lastIndex){
            if(motIntent[i]==motSecret[j] && teclat.indexOf(motIntent[i].capitalize()) != -1) teclat[teclat.indexOf(motIntent[i].capitalize())]= groc + motIntent[i] + reset //afageixo el color taronja a la lletra en la llista teclat
            if(motIntent[j].length==1){
                var countLetter = motSecret.count { it == motSecret[j] }
                var yelowRepeat = 0
                if(motIntent[i]==motSecret[j] &&  motIntent[j] != motSecret[j] && yelowCheck( motSecret, motIntent, motIntent[i].single())){
                    motIntent[i]= groc + motIntent[i] + reset
                    yelowRepeat+=1
                    lletra= true
                }
            }
            else if(motIntent[j].length>1){
                if(motIntent[i]==motSecret[j] &&  motIntent[j] != verd + motSecret[j] + reset){
                    motIntent[i]= groc + motIntent[i] + reset
                    lletra= true
                }
            }


        }
        if(!lletra && teclat.indexOf(motIntent[i].capitalize()) != -1) teclat[teclat.indexOf(motIntent[i].capitalize())]= negre + motIntent[i] + reset //afageixo el color negre a la lletra en la llista teclat


    }
    return (Pair(motIntent,teclat))
}

/**
 * The fuction prints the intents previously colored
 * @param motIntent MutableList<String> each string is a word of the intent
 */

fun attemptPrint(motIntent: MutableList<String>){
    for(i in 0..motIntent.lastIndex){// printejar el resultat
        print(motIntent[i])
    }

    println()
}

/**
 *The function check if the attempt word equals to the secret words to know if the game should continue or end
 * @param
 * @return
 */

fun attemptCheck( stringMotIntent: String, stringMotSecret: String, language: Int, intents: Int ):Boolean{
    var successful = false
    if(stringMotIntent==stringMotSecret){
        when(language){
            1->println("Molt bé! has encertat!")
            2->println("Nice! you guessed the word!")
            3->println("Muy bien! Has acertado!")
        }

        successful = true
    }
    else{
        repeat(5) {println()}
        if(intents in 2..5){
            when(language){
                1->println("Torna a intentar-ho")
                2->println("Try again")
                3->println("Vuelve a intentarlo")
            }
        }else if(intents == 1){
            when(language){
                1->println("La paraula era $stringMotSecret")
                2->println("The word was $stringMotSecret")
                3->println("La palabra era $stringMotSecret")
            }
        }

    }
    return successful
}

/**
 * @param
 * @return
 */

fun updatePreviousAttempts( motIntent: MutableList<String>, intentsAnteriors: MutableList<MutableList<String>>, intents: Int){
    for(i in 0..motIntent.lastIndex){
        intentsAnteriors[6-intents][i]=motIntent[i]
    }
}

/**
 * @param
 * @return
 */

fun repeatRankingOrEndGame(intents:Int, language:Int): Int{
    var partida = 0
    val scanner = Scanner(System. `in`)
    if(intents==0){
        when (language) {
            1 ->println("Ups, has perdut, però pots tornar a intenta-ho.\n ")

            2 ->println("Oops, you lost, but you can try again.\n ")

            3 ->println("Ups, has perdido, pero puedes volver a intentarlo.\n ")

         }
    }
    when (language) {
        1 ->println("Que vols fer?: \n -Per començar una nova partida: 2 \n -Ranking: 1 \n -Per finalitzar joc: 0")

        2 ->println("What would you like to do?: \n -To start a new game: 2 \n -Ranking: 1 \n -To end the game: 0")

        3 ->println("¿Qué quieres hacer?: \n -Para empezar una nueva partida: 2 \n -Ranking: 1\n -Para finalizar juego: 0")

    }
    var partidaString = scanner.next()

    while( partidaString !="2" && partidaString != "1" && partidaString != "0"){
        when(language){
            1->println("Nomès pots introduir 2,1 o 0 !")
            2->println("You can only intrude  2,1 or 0 !")
            3->println("Solo puedes introducir 2,1 o 0 !")
        }
        partidaString = scanner.next()
    }
    return(partidaString.toInt())
}

/**
 * @param
 * @return
 */

fun ranking(intents: Int, language:Int, name: String, win: Boolean, print: Boolean):Int{
    var rankingPath =Path("./src/main/languages/")

    when(language){
        1->rankingPath= Path("./src/main/ranking/rankingCat.txt")
        2->rankingPath= Path("./src/main/ranking/rankingEng.txt")
        3->rankingPath= Path("./src/main/ranking/rankingSpa.txt")
    }

    var oldRanking=rankingPath.readLines().toMutableList()
    var newRankingList: MutableList<String> = oldRanking

    if(win){
        when(language){
            1-> {
                newRankingList.add("$name,${6-intents}")
            }
            2->{

                newRankingList.add("$name,${6-intents}")
            }
            3->{

                newRankingList.add("$name,${6-intents}")
            }


        }
        if(newRankingList.size > 1){
            newRankingList = newRankingList.sortedBy { (it.split(",")[1])}.toMutableList()//reversed().
        }
//    rankingPath.writeText() newRanking.

//    for(i in newRankingList.lastIndex downTo 1){
//        if(newRankingList[i].split("")[newRankingList[i].split("").lastIndex-1] >= newRankingList[i-1].split("")[newRankingList[i-1].split("").lastIndex-1]){
//            val switch = newRankingList[i]
//            newRankingList[i] = newRankingList[i-1]
//            newRankingList[i-1]=switch
////            Collections.swap(oldRanking, oldRanking[i],oldRanking[i-1])
//        }
//    }
    }

    var newRanking=""

    for(i in 0..newRankingList.lastIndex){
        print("${newRankingList[i]}\n")


//        when(i){
//            0->{
//                print("\uD83E\uDD47 ${newRankingList[i]}\n")
//            }
//            1->{
//                print("\uD83E\uDD48 ${newRankingList[i]}\n")
//            }
//            2->{
//                print("\uD83E\uDD49 ${newRankingList[i]}\n")
//            }
//            else->{
//                print("${newRankingList[i]}\n")
//            }
//        }

        newRanking+= "${newRankingList[i]}\n"
    }


    rankingPath.writeText(newRanking)

    var partida = repeatRankingOrEndGame(intents,language)
    return(partida)
}


fun yelowCheck(motSecret: MutableList<String>, motIntent:MutableList<String>, letter: Char): Boolean{
    var count = 0
    val groc = "\u001B[43m"
    val reset = "\u001B[00m"
    val yellow = groc + letter + reset
    for(i in 0 until motIntent.lastIndex){
        if(motIntent[i]==yellow){
            count++
        }
    }

    val letterMotSecret = motSecret.toString().count{ it == letter}
    return(count < letterMotSecret)
}