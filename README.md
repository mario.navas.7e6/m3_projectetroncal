### M3, Projecte Troncal

# WORDLE

## Description
Wordle is a word guessing game. Each game you have to guess a new word. You have 6 attempts and each attempt you make reveals some information. It is inspired in [wordle](https://wordlegame.org/es). But this one is to play in the IDE's console.

## Instructions
The game plays like the old ‘mastermind’ board game, but with letters
instead of coloured pins. The gameplay is as follows:

1.  Enter a word as a guess for the hidden target word.
2.  Any letters which are within the hidden target word are coloured in
    yellow.
3.  Any letters which match exactly the letter in the hidden target word
    are coloured green
4.  Figure out a new candidate word as a guess for the hidden target
    word, and go back to Step 1.


## Installation
Just clone the project, and run the main document **Main.kt** in Intellij IDEA.

## Support
If you have any questions or suggestions, contact me by email **mario.nava.7E6@itb.cat**

## License
The project has a MIT License

## Project status
The project is in current development. I have in mind that the project is deficient or improvable in many areas, such as:

1. Improve the visual presentation.
2. Improve de dokka documentation.
3. Improve the lists of secret Words.
4. Implementation of a Ranking.
5. Implementation of Dictionaries to check words existence.
6. Add Languages.